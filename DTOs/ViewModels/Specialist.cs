﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.ViewModels
{
    public class Specialist:BaseUser
    {
        public string Surname { get; set; }
        public string Information { get; set; }
        public string Photos { get; set; }
    }
}
