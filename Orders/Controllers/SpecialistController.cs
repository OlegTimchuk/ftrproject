﻿using Dal;
using Dal.Models;
using Dal.Contract;
using DTOs.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Orders.Controllers
{
    [ApiController]
    [Route("specialist")]
    public class SpecialistController : Controller
    {
        private readonly IBaseRepository _baseRepository;
        public SpecialistController(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        [HttpGet]
        public JsonResult Get()
        {
            string query = @"
                    select Id, Name, Surname, Password, Phone, Information, Photos , Email
                    from dbo.Specialists";
            DataTable table = _baseRepository.ExecuteScript(query);

            return new JsonResult(table);
        }

        [HttpPost]
        public JsonResult Post(Specialist  specialist)
        {
            string query = @"
                    insert into dbo.Specialists 
                    (Name, Surname, Password,Phone,Information,Photos,Email)
                    values 
                    (
                    '" + specialist.Name + @"'
                    ,'" + specialist.Surname + @"'
                    ,'" + specialist.Password + @"'
                     ,'" + specialist.Phone + @"'
                    ,'" + specialist.Information + @"'
                    ,'" + specialist.Photos + @"'
                    ,'" + specialist.Email + @"'
                    )
                    ";
            DataTable table = _baseRepository.ExecuteScript(query);

            return new JsonResult("Added Successfully");
        }


        [HttpPut]
        public JsonResult Put(Specialist specialist)
        {
            string query = @"
                    update dbo.Specialists set 
                     Name = '" + specialist.Name + @"'
                    ,Surname = '" + specialist.Surname + @"'
                    ,Password = '" + specialist.Password + @"'
                    ,Phone = '" + specialist.Phone + @"'
                    ,Information = '" + specialist.Information + @"'
                    ,Photos = '" + specialist.Photos + @"'
                    ,Email = '" + specialist.Email + @"'
                    where Id = " + specialist.Id + @"
                    ";
            DataTable table = _baseRepository.ExecuteScript(query);

            return new JsonResult("Updated Successfully");
        }

        [HttpDelete("{id}")]
        public JsonResult Delete(int Id)
        {
            string query = @"
                    delete from dbo.Specialists
                    where Id = " + Id + @" 
                    ";
            DataTable table = _baseRepository.ExecuteScript(query);

            return new JsonResult("Deleted Successfully");
        }
    }
}
