﻿using Bll.Sort;
using Dal;
using Dal.Models;
using Dapper;
using Dal.Contract;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Orders.Controllers
{
    [Route("sortcategory")]
    [ApiController]
    public class SortCategoryController : ControllerBase
    {
        public ISortRepository repozitory { get; set; }
        public SortCategoryController(IConfiguration configuration)
        {
            SortRepository repo = new SortRepository(configuration);
            repozitory = repo;
        }


        [HttpGet]
        public JsonResult Index()
        {
            return new JsonResult(repozitory.SortCategory());
        }

    }
}
