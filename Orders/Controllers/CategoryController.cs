﻿using Dal;
using Dal.Models;
using Dal.Contract;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Orders.Controllers
{
    [ApiController]
    [Route("сategory")]
    public class CategoryController : Controller
    {
        private readonly IBaseRepository _baseRepository;
        public CategoryController(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        [HttpGet]
        public JsonResult Get()
        {
            string query = @"
                    select * 
                    from dbo.Categories";
            DataTable table = _baseRepository.ExecuteScript(query);

            return new JsonResult(table);
        }
        [HttpPost]
        public JsonResult Post(Category cat)
        {
            string query = @"
                    insert into dbo.Categories 
                    (Name)
                    values 
                    (
                    '" + cat.Name + @"'
                    )
                    ";
            DataTable table = _baseRepository.ExecuteScript(query);

            return new JsonResult("Added Successfully");
        }

        [HttpPut]
        public JsonResult Put(Category cat)
        {
            string query = @"
                    update dbo.Categories set 
                     Name = '" + cat.Name + @"'
                     where Id = " + cat.Id + @"
                    ";

            DataTable table = _baseRepository.ExecuteScript(query);

            return new JsonResult("Updated Successfully");
        }

        [HttpDelete("{id}")]
        public JsonResult Delete(int id)
        {
            string query = @"
                    delete from dbo.Categories
                    where Id = " + id + @" 
                    ";

            DataTable table = _baseRepository.ExecuteScript(query);

            return new JsonResult("Deleted Successfully");
        }
    }
}
