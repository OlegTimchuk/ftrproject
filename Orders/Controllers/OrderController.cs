﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dal.Models;
using Dal;
using Dal.Contract;

namespace Orders.Controllers
{
    [ApiController]
    [Route("order")]
    public class OrderController : Controller
    {
        private readonly IBaseRepository _baseRepository;
        public OrderController(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        [HttpGet]
        public JsonResult GetOrder()
        {
            string query = @"
                    select Id, Name, Details, City, Address, Fulfillment, Price , PaymentMethod, Specialist, Client 
                    from dbo.Orders";
            DataTable table = _baseRepository.ExecuteScript(query);

            return new JsonResult(table);
        }

        [HttpPost]
        public JsonResult Post(Order order)
        {
            string query = @"
                    insert into dbo.Orders 
                    (Name,Details,City,Address,Fulfillment,Price,PaymentMethod,Specialist,Client)
                    values 
                    (
                    '" + order.Name + @"'
                    ,'" + order.Details + @"'
                    ,'" + order.City + @"'
                    ,'" + order.Address + @"'
                    ,'" + order.Fulfillment + @"'
                    ,'" + order.Price + @"'
                    ,'" + order.PaymentMethod + @"'
                    )
                    ";
            DataTable table = _baseRepository.ExecuteScript(query);

            return new JsonResult("Added Successfully");
        }

        [HttpPut]
        public JsonResult PutOrder(Order order)
        {
            string query = @$"
                    update dbo.Orders set 
                     Name = '{order.Name}'
                    ,Details = '{order.Details}'
                    ,City = '{order.City}'
                    ,Address = '{order.Address}'
                    ,Fulfillment = '" + order.Fulfillment + @"'
                    ,Price = '" + order.Price + @"'
                    ,PaymentMethod = '" + order.PaymentMethod + @"'

                    where Id = " + order.Id + @"
                    ";
            DataTable table = _baseRepository.ExecuteScript(query);

            return new JsonResult("Updated Successfully");
        }

        [HttpDelete("{id}")]
        public JsonResult DeleteOrder(int id)
        {
            string query = @"
                    delete from dbo.Orders
                    where Id = " + id + @" 
                    ";
            DataTable table = _baseRepository.ExecuteScript(query);

            return new JsonResult("Deleted Successfully");
        }
    }        
}
