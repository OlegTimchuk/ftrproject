﻿using Dal;
using Dal.Contract;
using Dal.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Orders.Controllers
{
    [ApiController]
    [Route("category/subcategory")]
    public class SubcategoryController : Controller
    {
        private readonly IBaseRepository _baseRepository;
        public SubcategoryController(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        [HttpGet]
        public JsonResult Get()
        {
            string query = @"
                    select Id, Name, Category
                    from dbo.Subcategories";
            DataTable table = _baseRepository.ExecuteScript(query);

            return new JsonResult(table);
        }

        [HttpPost]
        public JsonResult Post(Subcategory subcategory)
        {
            string query = @"
                    insert into dbo.Subcategories 
                    (Name, Category)
                    values 
                    (
                    '" + subcategory.Name + @"'
                    ,'" + subcategory.Category.Id + @"'
                  
                    )
                    ";
            DataTable table = _baseRepository.ExecuteScript(query);

            return new JsonResult("Added Successfully");
        }


        [HttpPut]
        public JsonResult Put(Subcategory subcategory)
        {
            string query = @"
                    update dbo.Subcategories set 
                     Name = '" + subcategory.Name + @"'
                    ,Surname = '" + subcategory.Category.Id + @"'
                 
                    where Id = " + subcategory.Id + @"
                    ";
            DataTable table = _baseRepository.ExecuteScript(query);

            return new JsonResult("Updated Successfully");
        }

        [HttpDelete("{id}")]
        public JsonResult Delete(int Id)
        {
            string query = @"
                    delete from dbo.Subcategories
                    where Id = " + Id + @" 
                    ";
            DataTable table = _baseRepository.ExecuteScript(query);

            return new JsonResult("Deleted Successfully");
        }
    }
}
