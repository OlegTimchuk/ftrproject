﻿using Dal;
using Dal.Contract;
using Dal.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Orders.Controllers
{
    [ApiController]
    [Route("service")]
    public class ServiceController : Controller
    {
        private readonly IBaseRepository _baseRepository;
        private readonly ISearchRepository _searchRepository;
        public ServiceController(IBaseRepository baseRepository, ISearchRepository searchRepository)
        {
            _baseRepository = baseRepository;
            _searchRepository = searchRepository;
        }

        [HttpGet]
        public JsonResult Get()
        {
            string query = @"
                    select Id, Name, Description, Price, Specialist, Subcategory
                    from dbo.Services";
            DataTable table = _baseRepository.ExecuteScript(query);

            return new JsonResult(table);

        }

        [HttpGet]
        [Route("search/{searchString?}")]
        public JsonResult SearchServices(string searchString)
        {
            string query = @"
                    select * 
                    from dbo.Services";
            DataTable table = _baseRepository.ExecuteScript(query);

            if (!String.IsNullOrEmpty(searchString))
            {
                return new JsonResult(_searchRepository.SearchServices(searchString));
            }

            return new JsonResult(table);
        }

        [HttpPost]
        public JsonResult Post(Service serv)
        {
            string query = @"
                    insert into dbo.Services
                    (Name,Description,Price,Specialist,Subcategory)
                    values 
                    (
                    ,'" + serv.Name + @"'
                    ,'" + serv.Description + @"'
                    ,'" + serv.Price + @"'
                    ,'" + serv.Subcategory + @"'
                    )
                    ";
            DataTable table = _baseRepository.ExecuteScript(query);

            return new JsonResult("Added Successfully");
        }

        [HttpPut]
        public JsonResult Put(Service serv)
        {
            string query = @"
                    update dbo.Services set 
                     Name = '" + serv.Name + @"'
                    ,Description = '" + serv.Description + @"'
                    ,Price = '" + serv.Price + @"'
                    ,Subcategory = '" + serv.Subcategory + @"'
                    
                    where Id = " + serv.Id + @"
                    ";
            DataTable table = _baseRepository.ExecuteScript(query);

            return new JsonResult("Updated Successfully");
        }
        [HttpDelete("{id}")]
        public JsonResult Delete(int id)
        {
            string query = @"
                    delete from dbo.Services
                    where Id = " + id + @" 
                    ";
            DataTable table = _baseRepository.ExecuteScript(query);

            return new JsonResult("Deleted Successfully");
        }
    }
}
