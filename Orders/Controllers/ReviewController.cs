﻿using Dal;
using Dal.Models;
using Dal.Contract;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Orders.Controllers
{
    [ApiController]
    [Route("review")]
    public class ReviewController : Controller
    {
        private readonly IBaseRepository _baseRepository;
        public ReviewController(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        [HttpGet]
        public JsonResult GetReview()
        {
            string query = @"
                    select Id, Client, Rating, ReviewText, Date, Order
                    from dbo.Reviews";
            DataTable table = _baseRepository.ExecuteScript(query);

            return new JsonResult(table);
        }

        [HttpPost]
        public JsonResult Post(Review review)
        {
            string query = @"
                    insert into dbo.Reviews 
                    (Client,Rating,ReviewText,Date, Orders)
                    values 
                    (
                    ,'" + review.Rating + @"'
                    ,'" + review.ReviewText + @"'
                    ,'" + review.Date + @"'
                    ,'" + review.Order.Id + @"'
                    )
                    ";
            DataTable table = _baseRepository.ExecuteScript(query);

            return new JsonResult("Added Successfully");
        }

        [HttpPut]
        public JsonResult Put(Review review)
        {
            string query = @"
                    update dbo.Reviews set 
                    ,Rating = '" + review.Rating + @"'
                    ,ReviewText = '" + review.ReviewText + @"'
                    ,Date = '" + review.Date + @"'
                    ,Orders = '" + review.Order.Id + @"'

                    where Id = " + review.Id + @"
                    ";
            DataTable table = _baseRepository.ExecuteScript(query);

            return new JsonResult("Updated Successfully");
        }

        [HttpDelete("{id}")]
        public JsonResult Delete(int id)
        {
            string query = @"
                    delete from dbo.Reviews
                    where Id = " + id + @" 
                    ";
            DataTable table = _baseRepository.ExecuteScript(query);

            return new JsonResult("Deleted Successfully");
        }
    }
}
