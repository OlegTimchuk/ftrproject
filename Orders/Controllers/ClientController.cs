﻿using Dal;
using Dal.Models;
using DTOs.ViewModels;
using Dal.Contract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Orders.Controllers
{
    [ApiController]
    [Route("client")]
    public class ClientController : Controller
    {
        private readonly IBaseRepository _baseRepository;
        public ClientController(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        [HttpGet]
        public JsonResult Get()
        {
            string query = @"
                    select Id, Name, Email, Phone, Password
                    from dbo.AspNetUsers";
            DataTable table = _baseRepository.ExecuteScript(query);

            return new JsonResult(table);
        }

        [HttpPost]
        public JsonResult Post(Client client)
        {
            string query = @"
                    insert into dbo.Clients 
                    (Name,Email,Phone,Password)
                    values 
                    (
                    '" + client.Name + @"'
                    ,'" + client.Email + @"'
                    ,'" + client.Phone + @"'
                    ,'" + client.Password + @"'
                    
                    )
                    ";
            DataTable table = _baseRepository.ExecuteScript(query);
            return new JsonResult("Added Successfully");
        }


        [HttpPut]
        public JsonResult Put(Client client)
        {
            string query = @"
                    update dbo.Clients set 
                     Name = '" + client.Name + @"'
                    ,Email = '" + client.Email + @"'
                    ,Phone = '" + client.Phone + @"'
                    ,Password = '" + client.Password + @"'
                  
                    where Id = " + client.Id + @"
                    ";
            DataTable table = _baseRepository.ExecuteScript(query);

            return new JsonResult("Updated Successfully");
        }

        [HttpDelete("{id}")]
        public JsonResult Delete(int Id)
        {
            string query = @"
                    delete from dbo.Clients
                    where Id = " + Id + @" 
                    ";
            DataTable table = _baseRepository.ExecuteScript(query);

            return new JsonResult("Deleted Successfully");
        }
    }
}




