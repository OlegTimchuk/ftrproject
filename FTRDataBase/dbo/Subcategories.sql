﻿CREATE TABLE [dbo].[Subcategories](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] NVARCHAR(50) NOT NULL,
	[Category] [int] NOT NULL,
 CONSTRAINT [PK_Subcategories] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
),
CONSTRAINT [FK_Subcategory_Categories] FOREIGN KEY([Category])
REFERENCES [dbo].[Categories] ([Id])
)
