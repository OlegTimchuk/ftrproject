﻿CREATE TABLE [dbo].[Services](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] NVARCHAR(50) NOT NULL,
	[Description] [text] NOT NULL,
	[Price] [decimal](10, 2) NOT NULL,
	[Specialist] [int] NOT NULL,
	[Subcategory] [int] NOT NULL,
 CONSTRAINT [PK_Services] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
),
CONSTRAINT [FK_Services_Subcategories] FOREIGN KEY([Subcategory])
REFERENCES [dbo].[Subcategories] ([Id])
)