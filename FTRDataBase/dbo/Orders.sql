﻿CREATE TABLE [dbo].[Orders](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] NVARCHAR(50) NOT NULL,
	[Details] NVARCHAR(50) NOT NULL,
	[City] NVARCHAR(50) NOT NULL,
	[Address] NVARCHAR(50) NOT NULL,
	[Fulfillment] [datetime] NOT NULL,
	[Price] [decimal](10, 2) NOT NULL,
	[PaymentMethod] NVARCHAR(50) NOT NULL,
	[Specialist] [int] NULL,
	[Client] [int] NOT NULL,
 CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)

)
