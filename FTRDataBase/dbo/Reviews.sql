﻿CREATE TABLE [dbo].[Reviews](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Client] [int] NOT NULL,
	[Rating] [int] NOT NULL,
	[ReviewText] [text] NOT NULL,
	[Date] [datetime] NOT NULL,
	[Order] [int] NOT NULL,
 CONSTRAINT [PK_Reviews] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
),
CONSTRAINT [FK_Reviews_Orders] FOREIGN KEY([Order])
REFERENCES [dbo].[Orders] ([Id])
)