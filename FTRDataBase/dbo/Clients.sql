﻿CREATE TABLE [dbo].[Clients](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] NVARCHAR(50) NOT NULL,
	[Email] NVARCHAR(50) NULL,
	[Phone] NVARCHAR(50) NULL,
 [NormalizedUserName] NVARCHAR(50) NOT NULL, 
    [NormalizedEmail] NVARCHAR(50) NOT NULL, 
    [Password] NVARCHAR(50) NOT NULL, 
    [PhoneNumberConfirmed ] BIT NOT NULL, 
    [TwoFactorEnabled] BIT NOT NULL, 
    [EmailConfirmed] BIT NOT NULL, 
    CONSTRAINT [PK_Clients1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
