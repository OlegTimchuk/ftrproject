﻿using Dal.Contract;
using Dal.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;


namespace Dal
{
    public class BaseRepository:IBaseRepository
    {
        private readonly IConfiguration _configuration;
     
        public BaseRepository(IConfiguration configuration)
        {
            _configuration = configuration;

        }
        public  DataTable ExecuteScript(string query)
        {
            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("FTRAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader); ;

                    myReader.Close();
                    myCon.Close();
                }
            }
            return table;
        }
    }
}
