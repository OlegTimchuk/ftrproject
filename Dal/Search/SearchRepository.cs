﻿
using Dal.Contract;
using Dal.Models;
using Dapper;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Dal.Models
{
    public class SearchRepository : ISearchRepository
    {
        private readonly string _connectionString;
        public SearchRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("FTRAppCon");

        }

        
        public List<Service> SearchServices(string searchString)
        {
            List<Service> users = new List<Service>();
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                users = db.Query<Service>("SELECT * FROM Services").ToList();
                users = users.Where(s => s.Name.Contains(searchString)).ToList();
            }
            return users;
        }
    }
}
