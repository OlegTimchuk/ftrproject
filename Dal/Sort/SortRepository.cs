﻿
using Dal.Contract;
using Dal.Models;
using Dapper;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;


namespace Bll.Sort
{
    public  class SortRepository: ISortRepository
    {
        private readonly  string _connectionString;
       
        public SortRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("FTRAppCon"); 
              
        }
      
        public List<Category> SortCategory() 
        {
            List<Category> categories =new List<Category>();
           
            using (var db = new SqlConnection(_connectionString))
            {
                categories = db.Query<Category>(@"SELECT *
                                FROM  Categories categories INNER JOIN Subcategories subcategories
                                ON categories.Id=subcategories.Category
                                ORDER BY  categories.Name,subcategories.Name").ToList();
                               
            }
            return categories;
       }
    }
     
}

