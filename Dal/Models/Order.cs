﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Models
{
    public class Order
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Details { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public DateTime Fulfillment { get; set; }
        public decimal Price { get; set; }
        public string PaymentMethod { get; set; }
        //public Specialist Specialist { get; set; }
        //public Client Client { get; set; }
        


    }
}
