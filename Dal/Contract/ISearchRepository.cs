﻿using Dal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Contract
{
    public interface ISearchRepository
    {
        List<Service> SearchServices(string searchString);
    }
}
